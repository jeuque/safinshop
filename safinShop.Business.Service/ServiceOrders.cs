﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceOrders : IServiceOrders
    {
        private readonly IRepoOrder _repoOrders;
        private readonly IRepoOrderscontent _repoOrderscontent;
        private readonly IMapper _mapper;

        public ServiceOrders(IRepoOrder repoOrders, IMapper mapper, IRepoOrderscontent repoOrderscontent)
        {
            _repoOrders = repoOrders;
            _mapper = mapper;
            _repoOrderscontent = repoOrderscontent;
        }

        public async Task<OrdersReadDto> CreateOrdersAsync(OrdersAddDto ordersToAdd)
        {
            var ordersAdd = _mapper.Map<Orders>(ordersToAdd);
            var orders = await _repoOrders.CreateElementAsync(ordersAdd).ConfigureAwait(false);

            return _mapper.Map<OrdersReadDto>(orders);
        }

        public async Task<int> DeleteOrdersByIdAsync(int ordersId)
        {
            var ordersToDelete = await _repoOrders.GetByKeyAsync(ordersId).ConfigureAwait(false);
            var orders = await _repoOrders.DeleteElementAsync(ordersToDelete).ConfigureAwait(false);

            return orders.OrdersId;
        }

        public async Task<OrdersReadDetailsDto> GetOrdersByIdAsync(int ordersId)
        {
            var orders = await _repoOrders.GetByKeyAsyncDetails(ordersId).ConfigureAwait(false);
            var content = await _repoOrderscontent.GetByOrdersDetailsAsync(ordersId).ConfigureAwait(false);
            var ordersDto = _mapper.Map<OrdersReadDetailsDto>(orders);
            ordersDto.Orderscontent = _mapper.Map<IEnumerable<OrderscontentReadDetailsDto>>(content);
            double total = 0;
            foreach (var item in ordersDto.Orderscontent)
            {
                total = total + (item.Quantity * item.UnitePrice);
            }
            ordersDto.OrderTotal = total;
            return ordersDto;
        }

        public async Task<IEnumerable<OrdersReadDto>> GetListOrdersAsync()
        {
            var orders = await _repoOrders.GetAllAsyncDetails().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<OrdersReadDto>>(orders);
        }

        public async Task<OrdersReadDto> UpdateOrdersByIdAsync(int ordersId, OrdersUpdateDto ordersToUpdate)
        {
            var ordersUpdate = _mapper.Map<Orders>(ordersToUpdate);
            ordersUpdate.OrdersId = ordersId;
            var orders = await _repoOrders.UpdateElementAsync(ordersUpdate).ConfigureAwait(false);

            return _mapper.Map<OrdersReadDto>(orders);
        }
    }
}
