﻿namespace safinShop.Business.Model
{
    public class OrdersBaseDTO
    {
        public DateTime OrdersDate { get; set; }
        public int CustomerId { get; set; }
        public int OrdersStatusId { get; set; }

    }

    public class OrdersReadDto : OrdersBaseDTO
    {
        public int OrdersId { get; set; }
    }

    public class OrdersReadDetailsDto : OrdersReadDto
    {
        public IEnumerable<OrderscontentReadDetailsDto>? Orderscontent { get; set; }
        public double OrderTotal { get; set; }
    }

    public class OrdersAddDto : OrdersBaseDTO
    {
    }
    public class OrdersUpdateDto : OrdersBaseDTO
    {
    }
}