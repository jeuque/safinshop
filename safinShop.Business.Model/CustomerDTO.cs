﻿namespace safinShop.Business.Model
{
    public class CustomerBaseDTO
    {
        public string CustomerFisrtName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerMail { get; set; }

    }

    public class CustomerReadCompDto : CustomerBaseDTO
    {
        public int CustomerId { get; set; }
    }

    public class CustomerReadDto : CustomerReadCompDto
    {
        public AddressReadDto AddressReadDto { get; set; }
    }

    public class CustomerAddDto : CustomerBaseDTO
    {
        public string CustomerPassword { get; set; }
    }
    public class CustomerUpdateDto : CustomerBaseDTO
    {
        public string CustomerPassword { get; set; }
    }
    public class CustomerDeleteDto
    {
        public int CustomerId { get; set; }
    }
}