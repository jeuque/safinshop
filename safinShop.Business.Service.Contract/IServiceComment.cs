﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceComment
    {
        /// <summary>
        /// Creates the comment.
        /// </summary>
        /// <param name="commentToAdd">The comment to add.</param>
        /// <returns></returns>
        Task<CommentReadDto> CreateCommentAsync(CommentAddDto commentToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des comments.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<CommentReadDto>> GetListCommentAsync();

        /// <summary>
        /// Gets the comment by name asynchronous.
        /// </summary>
        /// <param name="commentName">Name of the comment.</param>
        /// <returns></returns>
        Task<CommentReadDto> GetCommentByIdAsync(int commentId);

        /// <summary>
        /// Updates the comment by identifier asynchronous.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <returns></returns>
        Task<CommentReadDto> UpdateCommentByIdAsync(int commentId, CommentUpdateDto commentToUpdate);

        /// <summary>
        /// Deletes the comment by identifier asynchronous.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <returns></returns>
        Task<int> DeleteCommentByIdAsync(int commentId);
    }
}