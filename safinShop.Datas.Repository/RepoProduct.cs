﻿using Microsoft.EntityFrameworkCore;
using safinShop.Datas.Context.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository
{
    public class RepoProduct : GenericRepo<Product>, IRepoProduct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepoProduct"/> class.
        /// </summary>
        /// <param name="safinDbContext"></param>
        public RepoProduct(ISafinDbContext safinDbContext) : base(safinDbContext)
        {
        }

        /// <summary>
        /// Gets all asynchronous details.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Product>> GetAllAsyncDetails()
        {
            return await _table
                .Include(p => p.Category)
                .ToListAsync().ConfigureAwait(false);
        }

        public async Task<Product?> GetByKeyAsyncDetails(int id)
        {
            return await _table
                .Include(p => p.Category)
                .FirstOrDefaultAsync(p => p.ProductId == id).ConfigureAwait(false);
        }
    }
}
