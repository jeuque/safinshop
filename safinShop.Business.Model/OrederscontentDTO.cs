﻿namespace safinShop.Business.Model
{
    public class OrderscontentBaseDTO
    {
        public int Quantity { get; set; }
        public int OrdersId { get; set; }

    }

    public class OrderscontentReadDto : OrderscontentBaseDTO
    {
        public int OrderscontentId { get; set; }
        public double UnitePrice { get; set; }
    }

    public class OrderscontentReadDetailsDto : OrderscontentReadDto 
    {
        public ProductReadDto? Product { get; set; }
    }

    public class OrderscontentAddDto : OrderscontentBaseDTO
    {
        public int ProductId { get; set; }
    }
    public class OrderscontentUpdateDto : OrderscontentBaseDTO
    {
        public int ProductId { get; set; }
    }
}