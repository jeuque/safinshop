﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        /// <summary>
        /// The service orders
        /// </summary>
        private readonly IServiceOrders _serviceOrders;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersController"/> class.
        /// </summary>
        /// <param name="serviceOrders">The service orders.</param>
        public OrdersController(IServiceOrders serviceOrders)
        {
            _serviceOrders = serviceOrders;
        }

        // GET : API/orders
        /// <summary>
        /// Gets the orders list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<OrdersReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetOrderssAsync()
        {
            var orders = await _serviceOrders.GetListOrdersAsync().ConfigureAwait(false);

            return Ok(orders);
        }

        // // GET : API/orders/:ordersid
        /// <summary>
        /// Gets the orders by identifier asynchronous.
        /// </summary>
        /// <param name="ordersId">The orders identifier.</param>
        /// <returns></returns>
        [HttpGet("{ordersId}")]
        [ProducesResponseType(typeof(OrdersReadDetailsDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetOrdersByIdAsync(int ordersId)
        {
            var orders = await _serviceOrders.GetOrdersByIdAsync(ordersId).ConfigureAwait(false);

            return Ok(orders);
        }

        /// <summary>
        /// Posts the orders asynchronous.
        /// </summary>
        /// <param name="newOrders">The new orders.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(OrdersReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostOrdersAsync(OrdersAddDto newOrders)
        {
            var orders = await _serviceOrders.CreateOrdersAsync(newOrders).ConfigureAwait(false);

            return Ok(orders);
        }

        /// <summary>
        /// Updates the orders by identifier asynchronous.
        /// </summary>
        /// <param name="ordersId">The orders identifier.</param>
        /// <param name="updatedOrders">The updated orders.</param>
        /// <returns></returns>
        [HttpPut("{ordersId}")]
        [ProducesResponseType(typeof(OrdersReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateOrdersByIdAsync(int ordersId, OrdersUpdateDto updatedOrders)
        {
            var orders = await _serviceOrders.UpdateOrdersByIdAsync(ordersId, updatedOrders).ConfigureAwait(false);

            return Ok(orders);
        }

        /// <summary>
        /// Deletes the orders by identifier asynchronous.
        /// </summary>
        /// <param name="ordersId">The orders identifier.</param>
        /// <returns></returns>
        [HttpDelete("{ordersId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteOrdersByIdAsync(int ordersId)
        {
            var ordersIdDeleted = await _serviceOrders.DeleteOrdersByIdAsync(ordersId).ConfigureAwait(false);

            return Ok();
        }

    }
}
