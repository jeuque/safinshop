﻿namespace safinShop.Business.Model
{
    public class ProductBaseDTO
    {
        public string ProductName { get; set; } = null!;
        public double ProductPrice { get; set; }
        public int ProductStock { get; set; }
        public string ProductDescription { get; set; } = null!;

    }

    public class ProductReadDto : ProductBaseDTO
    {
        public int ProductId { get; set; }
        public CategoryReadDto Category { get; set; } = null!;
    }

    public class ProductAddDto : ProductBaseDTO
    {
        public int CategoryId { get; set; }
    }
    public class ProductUpdateDto : ProductBaseDTO
    {
        public int CategoryId { get; set; }
    }
}