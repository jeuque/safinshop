﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceOrdersstatus : IServiceOrdersstatus
    {
        private readonly IRepoOrdersstatus _repoOrdersstatus;
        private readonly IMapper _mapper;

        public ServiceOrdersstatus(IRepoOrdersstatus repoOrdersstatus, IMapper mapper)
        {
            _repoOrdersstatus = repoOrdersstatus;
            _mapper = mapper;
        }

        public async Task<OrdersstatusReadDto> CreateOrdersstatusAsync(OrdersstatusAddDto ordersstatusToAdd)
        {
            var ordersstatusAdd = _mapper.Map<Ordersstatus>(ordersstatusToAdd);
            var ordersstatus = await _repoOrdersstatus.CreateElementAsync(ordersstatusAdd).ConfigureAwait(false);

            return _mapper.Map<OrdersstatusReadDto>(ordersstatus);
        }

        public async Task<int> DeleteOrdersstatusByIdAsync(int ordersstatusId)
        {
            var ordersstatusToDelete = await _repoOrdersstatus.GetByKeyAsync(ordersstatusId).ConfigureAwait(false);
            var ordersstatus = await _repoOrdersstatus.DeleteElementAsync(ordersstatusToDelete).ConfigureAwait(false);

            return ordersstatus.OrdersStatusId;
        }

        public async Task<OrdersstatusReadDto> GetOrdersstatusByIdAsync(int ordersstatusId)
        {
            var ordersstatus = await _repoOrdersstatus.GetByKeyAsync(ordersstatusId).ConfigureAwait(false);
            return _mapper.Map<OrdersstatusReadDto>(ordersstatus);
        }

        public async Task<IEnumerable<OrdersstatusReadDto>> GetListOrdersstatusAsync()
        {
            var ordersstatuss = await _repoOrdersstatus.GetAllAsync().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<OrdersstatusReadDto>>(ordersstatuss);
        }

        public async Task<OrdersstatusReadDto> UpdateOrdersstatusByIdAsync(int ordersstatusId, OrdersstatusUpdateDto ordersstatusToUpdate)
        {
            var ordersstatusUpdate = _mapper.Map<Ordersstatus>(ordersstatusToUpdate);
            ordersstatusUpdate.OrdersStatusId = ordersstatusId;
            var ordersstatus = await _repoOrdersstatus.UpdateElementAsync(ordersstatusUpdate).ConfigureAwait(false);

            return _mapper.Map<OrdersstatusReadDto>(ordersstatus);
        }
    }
}
