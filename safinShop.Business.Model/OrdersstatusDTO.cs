﻿namespace safinShop.Business.Model
{
    public class OrdersstatusBaseDTO
    {
        public string OrdersStatusName { get; set; } = null!;

    }

    public class OrdersstatusReadDto : OrdersstatusBaseDTO
    {
        public int OrdersstatusId { get; set; }
    }

    public class OrdersstatusAddDto : OrdersstatusBaseDTO
    {
    }
    public class OrdersstatusUpdateDto : OrdersstatusBaseDTO
    {
    }

}