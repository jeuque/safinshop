﻿using safinShop.Datas.Context.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository
{
    public class RepoLinkcustadd : GenericRepo<Linkcustadd>, IRepoLinkcustadd
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RepoLinkcustadd"/> class.
        /// </summary>
        /// <param name="safinDbContex"></param>
        public RepoLinkcustadd(ISafinDbContext safinDbContext) : base(safinDbContext)
        {
        }
    }
}
