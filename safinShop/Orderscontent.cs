﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Orderscontent
    {
        public int OrdersContentId { get; set; }
        public int Quantity { get; set; }
        public double UnitePrice { get; set; }
        public int OrdersId { get; set; }
        public int ProductId { get; set; }

        public virtual Orders Orders { get; set; } = null!;
        public virtual Product Product { get; set; } = null!;
    }
}
