﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceBasketcontent
    {
        /// <summary>
        /// Creates the basketcontent.
        /// </summary>
        /// <param name="basketcontentToAdd">The basketcontent to add.</param>
        /// <returns></returns>
        Task<BasketcontentReadDto> CreateBasketcontentAsync(BasketcontentAddDto basketcontentToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des basketcontents.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<BasketcontentReadDto>> GetListBasketcontentAsync();

        /// <summary>
        /// Gets the basketcontent by name asynchronous.
        /// </summary>
        /// <param name="basketcontentName">Name of the basketcontent.</param>
        /// <returns></returns>
        Task<BasketcontentReadDto> GetBasketcontentByIdAsync(int basketcontentId);

        /// <summary>
        /// Updates the basketcontent by identifier asynchronous.
        /// </summary>
        /// <param name="basketcontentId">The basketcontent identifier.</param>
        /// <returns></returns>
        Task<BasketcontentReadDto> UpdateBasketcontentByIdAsync(int basketcontentId, BasketcontentUpdateDto basketcontentToUpdate);

        /// <summary>
        /// Deletes the basketcontent by identifier asynchronous.
        /// </summary>
        /// <param name="basketcontentId">The basketcontent identifier.</param>
        /// <returns></returns>
        Task<int> DeleteBasketcontentByIdAsync(int basketcontentId);
    }
}