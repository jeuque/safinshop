﻿using Microsoft.EntityFrameworkCore;
using safinShop.Datas.Context.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository
{
    public class RepoOrderscontent : GenericRepo<Orderscontent>, IRepoOrderscontent
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RepoOrderscontent"/> class.
        /// </summary>
        /// <param name="safinDbContex"></param>
        public RepoOrderscontent(ISafinDbContext safinDbContext) : base(safinDbContext)
        {
        }

        public async Task<IEnumerable<Orderscontent>> GetByOrdersDetailsAsync(int ordersId)
        {
            return await _table
                            .Include(oc => oc.Orders)
                              .Where(o => o.OrdersId == ordersId)
                            .Include(oc => oc.Product)
                                .ThenInclude(p => p.Category)
                            .ToListAsync().ConfigureAwait(false);
        }

        public async Task<IEnumerable<Orderscontent>> GetAllAsyncDetails()
        {
            return await _table
                .Include(c => c.Product)
                    .ThenInclude(p => p.Category)
                .ToListAsync().ConfigureAwait(false);
        }

        public async Task<Orderscontent?> GetByKeyAsyncDetails(int id)
        {
            return await _table
                .Include(oc => oc.Product)
                    .ThenInclude(p => p.Category)
                .FirstOrDefaultAsync(oc => oc.OrdersContentId == id).ConfigureAwait(false);
        }
    }
}
