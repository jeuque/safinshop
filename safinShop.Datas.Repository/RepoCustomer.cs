﻿using safinShop.Datas.Context.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository
{
    public class RepoCustomer : GenericRepo<Customer>, IRepoCustomer
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RepoCustomer"/> class.
        /// </summary>
        /// <param name="safinDbContex"></param>
        public RepoCustomer(ISafinDbContext safinDbContext) : base(safinDbContext)
        {
        }
    }
}
