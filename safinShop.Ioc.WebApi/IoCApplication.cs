﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using safinShop.Business.Service;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Context.Contract;
using safinShop.Datas.Entities.safinShop.Datas.Context;
using safinShop.Datas.Repository;
using safinShop.Datas.Repository.Contract;

namespace safinShop.Ioc.WebApi
{
    public static class IoCApplication
    {
        /// <summary>
        /// Configures the injection dependency repository.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        public static IServiceCollection ConfigureInjectionDependencyRepository(this IServiceCollection services)
        {
            // Injections des Dépendances
            // - Repositories

            services.AddScoped<IRepoAddress, RepoAddress>();
            services.AddScoped<IRepoBasket, RepoBasket>();
            services.AddScoped<IRepoBasketcontent, RepoBasketcontent>();
            services.AddScoped<IRepoCategory, RepoCategory>();
            services.AddScoped<IRepoComment, RepoComment>();
            services.AddScoped<IRepoCustomer, RepoCustomer>();
            services.AddScoped<IRepoLinkcustadd, RepoLinkcustadd>();
            services.AddScoped<IRepoOrder, RepoOrder>();
            services.AddScoped<IRepoOrderscontent, RepoOrderscontent>();
            services.AddScoped<IRepoOrdersstatus, RepoOrdersstatus>();
            services.AddScoped<IRepoProduct, RepoProduct>();

            return services;
        }


        /// <summary>
        /// Configures the injection dependency service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        public static IServiceCollection ConfigureInjectionDependencyService(this IServiceCollection services)
        {
            // Injections des Dépendances
            // - Service

            services.AddScoped<IServiceCustomer, ServiceCustomer>();
            services.AddScoped<IServiceAddress, ServiceAddress>();
            services.AddScoped<IServiceCategory, ServiceCategory>();
            services.AddScoped<IServiceProduct, ServiceProduct>();
            services.AddScoped<IServiceComment, ServiceComment>();
            services.AddScoped<IServiceOrders, ServiceOrders>();
            services.AddScoped<IServiceOrderscontent, ServiceOrderscontent>();
            services.AddScoped<IServiceOrdersstatus, ServiceOrdersstatus>();
            services.AddScoped<IServiceBasket, ServiceBasket>();
            services.AddScoped<IServiceBasketcontent, ServiceBasketcontent>();
            services.AddScoped<IServiceLinkcustadd, ServiceLinkcustadd>();

            return services;
        }

        /// <summary>
        /// Configuration de la connexion de la base de données
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection ConfigureDBContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("BddConnection");

            services.AddDbContext<ISafinDbContext, SafinDBContext>(options => options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString))
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors());

            return services;
        }

    }
}