﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Ordersstatus
    {
        public Ordersstatus()
        {
            Orders = new HashSet<Orders>();
        }

        public int OrdersStatusId { get; set; }
        public string OrdersStatusName { get; set; } = null!;

        public virtual ICollection<Orders> Orders { get; set; }
    }
}
