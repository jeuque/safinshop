﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceOrderscontent
    {
        /// <summary>
        /// Creates the orderscontent.
        /// </summary>
        /// <param name="orderscontentToAdd">The orderscontent to add.</param>
        /// <returns></returns>
        Task<OrderscontentReadDto> CreateOrderscontentAsync(OrderscontentAddDto orderscontentToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des orderscontents.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<OrderscontentReadDto>> GetListOrderscontentAsync();

        /// <summary>
        /// Cette méthode permet de récupérer la liste des orderscontents en fonction de l'ordersId.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<OrderscontentReadDto>> GetListOrderscontentByOrdersAsync(int ordersId);

        /// <summary>
        /// Gets the orderscontent by name asynchronous.
        /// </summary>
        /// <param name="orderscontentName">Name of the orderscontent.</param>
        /// <returns></returns>
        Task<OrderscontentReadDto> GetOrderscontentByIdAsync(int orderscontentId);

        /// <summary>
        /// Updates the orderscontent by identifier asynchronous.
        /// </summary>
        /// <param name="orderscontentId">The orderscontent identifier.</param>
        /// <returns></returns>
        Task<OrderscontentReadDto> UpdateOrderscontentByIdAsync(int orderscontentId, OrderscontentUpdateDto orderscontentToUpdate);

        /// <summary>
        /// Deletes the orderscontent by identifier asynchronous.
        /// </summary>
        /// <param name="orderscontentId">The orderscontent identifier.</param>
        /// <returns></returns>
        Task<int> DeleteOrderscontentByIdAsync(int orderscontentId);
    }
}