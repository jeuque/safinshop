﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceProduct
    {
        /// <summary>
        /// Creates the product.
        /// </summary>
        /// <param name="productToAdd">The product to add.</param>
        /// <returns></returns>
        Task<ProductReadDto> CreateProductAsync(ProductAddDto productToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des products.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<ProductReadDto>> GetListProductAsync();

        /// <summary>
        /// Gets the product by name asynchronous.
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <returns></returns>
        Task<ProductReadDto> GetProductByIdAsync(int productId);

        /// <summary>
        /// Updates the product by identifier asynchronous.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        Task<ProductReadDto> UpdateProductByIdAsync(int productId, ProductUpdateDto productToUpdate);

        /// <summary>
        /// Deletes the product by identifier asynchronous.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        Task<int> DeleteProductByIdAsync(int productId);
    }
}