﻿using safinShop.Business.Model;
using safinShop.Datas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public static class MapperCustomer
    {
        /// <summary>
        /// Cette méthode permet de transformer un Client Entity en Client DTO.
        /// </summary>
        /// <param name="departement">The departement.</param>
        /// <returns></returns>
        public static CustomerReadDto TransformCustomerToReadDto(Customer customer, Address address)
        {
            CustomerReadDto customertRead = new CustomerReadDto()
            {
                CustomerId = customer.CustomerId,
                CustomerFisrtName = customer.CustomerFisrtName,
                CustomerLastName = customer.CustomerLastName,
                CustomerMail = customer.CustomerMail,
                AddressReadDto = new AddressReadDto()
                {
                    AddressId= address.AddressId,
                    AddressCity= address.AddressCity,
                    AddressName= address.AddressName,
                    AddressZipCode= address.AddressZipCode
                }
            };

            return customertRead;
        }

        /// <summary>
        /// Cette méthode permet de transformer un client DTO Update en client Entity.
        /// </summary>
        /// <param name="departement">The departement.</param>
        /// <returns></returns>
        public static Customer TransformUpdateDtoToEntity(CustomerUpdateDto customerToUpdate, int customerId)
        {
            Customer newCustomer = new Customer()
            {
                CustomerId = customerId,
                CustomerFisrtName = customerToUpdate.CustomerFisrtName,
                CustomerLastName = customerToUpdate.CustomerLastName,
                CustomerMail = customerToUpdate.CustomerMail,
                CustomerPassword = customerToUpdate.CustomerPassword
            };

            return newCustomer;
        }

        /// <summary>
        /// Cette méthode permet de transformer un client DTO Create en client Entity.
        /// </summary>
        /// <param name="departement">The departement.</param>
        /// <returns></returns>
        public static Customer TransformCreateDtoToEntity(CustomerAddDto CustomerToCreate)
        {
            Customer newCustomer = new Customer()
            {
                CustomerFisrtName = CustomerToCreate.CustomerFisrtName,
                CustomerLastName = CustomerToCreate.CustomerLastName,
                CustomerMail = CustomerToCreate.CustomerMail,
                CustomerPassword = CustomerToCreate.CustomerPassword
            };

            return newCustomer;
        }
    }
}
