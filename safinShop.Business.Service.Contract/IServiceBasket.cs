﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceBasket
    {
        /// <summary>
        /// Creates the basket.
        /// </summary>
        /// <param name="basketToAdd">The basket to add.</param>
        /// <returns></returns>
        Task<BasketReadDto> CreateBasketAsync(BasketAddDto basketToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des baskets.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<BasketReadDto>> GetListBasketAsync();

        /// <summary>
        /// Gets the basket by name asynchronous.
        /// </summary>
        /// <param name="basketName">Name of the basket.</param>
        /// <returns></returns>
        Task<BasketReadDto> GetBasketByIdAsync(int basketId);

        /// <summary>
        /// Updates the basket by identifier asynchronous.
        /// </summary>
        /// <param name="basketId">The basket identifier.</param>
        /// <returns></returns>
        Task<BasketReadDto> UpdateBasketByIdAsync(int basketId, BasketUpdateDto basketToUpdate);

        /// <summary>
        /// Deletes the basket by identifier asynchronous.
        /// </summary>
        /// <param name="basketId">The basket identifier.</param>
        /// <returns></returns>
        Task<int> DeleteBasketByIdAsync(int basketId);
    }
}