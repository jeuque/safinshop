﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinkcustaddController : ControllerBase
    {
        /// <summary>
        /// The service linkcustadd
        /// </summary>
        private readonly IServiceLinkcustadd _serviceLinkcustadd;

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkcustaddController"/> class.
        /// </summary>
        /// <param name="serviceLinkcustadd">The service linkcustadd.</param>
        public LinkcustaddController(IServiceLinkcustadd serviceLinkcustadd)
        {
            _serviceLinkcustadd = serviceLinkcustadd;
        }

        // GET : API/linkcustadds
        /// <summary>
        /// Gets the linkcustadds list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<LinkcustaddReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetLinkcustaddsAsync()
        {
            var linkcustadds = await _serviceLinkcustadd.GetListLinkcustaddAsync().ConfigureAwait(false);

            return Ok(linkcustadds);
        }

        // // GET : API/linkcustadd/:linkcustaddid
        /// <summary>
        /// Gets the linkcustadd by identifier asynchronous.
        /// </summary>
        /// <param name="linkcustaddId">The linkcustadd identifier.</param>
        /// <returns></returns>
        [HttpGet("{linkcustaddId}")]
        [ProducesResponseType(typeof(LinkcustaddReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetLinkcustaddByIdAsync(int linkcustaddId)
        {
            var linkcustadd = await _serviceLinkcustadd.GetLinkcustaddByIdAsync(linkcustaddId).ConfigureAwait(false);

            return Ok(linkcustadd);
        }

        /// <summary>
        /// Posts the linkcustadd asynchronous.
        /// </summary>
        /// <param name="newLinkcustadd">The new linkcustadd.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(LinkcustaddReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostLinkcustaddAsync(LinkcustaddAddDto newLinkcustadd)
        {
            var linkcustadd = await _serviceLinkcustadd.CreateLinkcustaddAsync(newLinkcustadd).ConfigureAwait(false);

            return Ok(linkcustadd);
        }

        /// <summary>
        /// Updates the linkcustadd by identifier asynchronous.
        /// </summary>
        /// <param name="linkcustaddId">The linkcustadd identifier.</param>
        /// <param name="updatedLinkcustadd">The updated linkcustadd.</param>
        /// <returns></returns>
        [HttpPut("{linkcustaddId}")]
        [ProducesResponseType(typeof(LinkcustaddReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateLinkcustaddByIdAsync(int linkcustaddId, LinkcustaddUpdateDto updatedLinkcustadd)
        {
            var linkcustadd = await _serviceLinkcustadd.UpdateLinkcustaddByIdAsync(linkcustaddId, updatedLinkcustadd).ConfigureAwait(false);

            return Ok(linkcustadd);
        }

        /// <summary>
        /// Deletes the linkcustadd by identifier asynchronous.
        /// </summary>
        /// <param name="linkcustaddId">The linkcustadd identifier.</param>
        /// <returns></returns>
        [HttpDelete("{linkcustaddId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteLinkcustaddByIdAsync(int linkcustaddId)
        {
            var linkcustaddIdDeleted = await _serviceLinkcustadd.DeleteLinkcustaddByIdAsync(linkcustaddId).ConfigureAwait(false);

            return Ok();
        }

    }
}
