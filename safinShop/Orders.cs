﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Orders
    {
        public Orders()
        {
            Orderscontent = new HashSet<Orderscontent>();
        }

        public int OrdersId { get; set; }
        public DateTime OrdersDate { get; set; }
        public int CustomerId { get; set; }
        public int OrdersStatusId { get; set; }

        public virtual Customer Customer { get; set; } = null!;
        public virtual Ordersstatus OrdersStatus { get; set; } = null!;
        public virtual ICollection<Orderscontent> Orderscontent { get; set; }
    }
}
