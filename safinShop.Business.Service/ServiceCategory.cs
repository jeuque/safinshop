﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceCategory : IServiceCategory
    {
        private readonly IRepoCategory _repoCategory;
        private readonly IMapper _mapper;

        public ServiceCategory(IRepoCategory repoCategory, IMapper mapper)
        {
            _repoCategory = repoCategory;
            _mapper = mapper;
        }

        public async Task<CategoryReadDto> CreateCategoryAsync(CategoryAddDto categoryToAdd)
        {
            var categoryAdd = _mapper.Map<Category>(categoryToAdd);
            var category = await _repoCategory.CreateElementAsync(categoryAdd).ConfigureAwait(false);

            return _mapper.Map<CategoryReadDto>(category);
        }

        public async Task<int> DeleteCategoryByIdAsync(int categoryId)
        {
            var categoryToDelete = await _repoCategory.GetByKeyAsync(categoryId).ConfigureAwait(false);
            var category = await _repoCategory.DeleteElementAsync(categoryToDelete).ConfigureAwait(false);

            return category.CategoryId;
        }

        public async Task<CategoryReadDto> GetCategoryByIdAsync(int categoryId)
        {
            var category = await _repoCategory.GetByKeyAsync(categoryId).ConfigureAwait(false);
            return _mapper.Map<CategoryReadDto>(category);
        }

        public async Task<IEnumerable<CategoryReadDto>> GetListCategoryAsync()
        {
            var categories = await _repoCategory.GetAllAsync().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<CategoryReadDto>>(categories);
        }

        public async Task<CategoryReadDto> UpdateCategoryByIdAsync(int categoryId, CategoryUpdateDto categoryToUpdate)
        {
            var categoryUpdate = _mapper.Map<Category>(categoryToUpdate);
            categoryUpdate.CategoryId = categoryId;
            var category = await _repoCategory.UpdateElementAsync(categoryUpdate).ConfigureAwait(false);

            return _mapper.Map<CategoryReadDto>(category);
        }
    }
}
