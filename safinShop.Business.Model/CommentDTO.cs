﻿namespace safinShop.Business.Model
{
    public class CommentBaseDTO
    {
        public string CommentTittle { get; set; }
        public string CommentContent { get; set; }
        public int CommentMark { get; set; }
    }

    public class CommentReadDto : CommentBaseDTO
    {
        public int CommentId { get; set; }
        public ProductReadDto Product { get; set; }
        public CustomerReadCompDto Customer { get; set; }
    }

    public class CommentAddDto : CommentBaseDTO
    {
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
    }
    public class CommentUpdateDto : CommentBaseDTO
    {
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
    }
}