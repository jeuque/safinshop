﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        /// <summary>
        /// The service product
        /// </summary>
        private readonly IServiceProduct _serviceProduct;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductController"/> class.
        /// </summary>
        /// <param name="serviceProduct">The service product.</param>
        public ProductController(IServiceProduct serviceProduct)
        {
            _serviceProduct = serviceProduct;
        }

        // GET : API/products
        /// <summary>
        /// Gets the products list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<ProductReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetProductsAsync()
        {
            var products = await _serviceProduct.GetListProductAsync().ConfigureAwait(false);

            return Ok(products);
        }

        // // GET : API/product/:productid
        /// <summary>
        /// Gets the product by identifier asynchronous.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("{productId}")]
        [ProducesResponseType(typeof(ProductReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetProductByIdAsync(int productId)
        {
            var product = await _serviceProduct.GetProductByIdAsync(productId).ConfigureAwait(false);

            return Ok(product);
        }

        /// <summary>
        /// Posts the product asynchronous.
        /// </summary>
        /// <param name="newProduct">The new product.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(ProductReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostProductAsync(ProductAddDto newProduct)
        {
            var product = await _serviceProduct.CreateProductAsync(newProduct).ConfigureAwait(false);

            return Ok(product);
        }

        /// <summary>
        /// Updates the product by identifier asynchronous.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="updatedProduct">The updated product.</param>
        /// <returns></returns>
        [HttpPut("{productId}")]
        [ProducesResponseType(typeof(ProductReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateProductByIdAsync(int productId, ProductUpdateDto updatedProduct)
        {
            var product = await _serviceProduct.UpdateProductByIdAsync(productId, updatedProduct).ConfigureAwait(false);

            return Ok(product);
        }

        /// <summary>
        /// Deletes the product by identifier asynchronous.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpDelete("{productId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteProductByIdAsync(int productId)
        {
            var productIdDeleted = await _serviceProduct.DeleteProductByIdAsync(productId).ConfigureAwait(false);

            return Ok();
        }

    }
}
