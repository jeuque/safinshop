﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Comment
    {
        public int CommentId { get; set; }
        public string CommentTittle { get; set; }
        public string CommentContent { get; set; }
        public int CommentMark { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; } = null!;
        public virtual Product Product { get; set; } = null!;
    }
}
