﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        /// <summary>
        /// The service customer
        /// </summary>
        private readonly IServiceCustomer _serviceCustomer;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerController"/> class.
        /// </summary>
        /// <param name="serviceCustomer">The service customer.</param>
        public CustomerController(IServiceCustomer serviceCustomer)
        {
            _serviceCustomer = serviceCustomer;
        }

        // GET : API/customers
        /// <summary>
        /// Gets the customers list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<CustomerReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCustomersAsync()
        {
            var customers = await _serviceCustomer.GetListCustomerAsync().ConfigureAwait(false);

            return Ok(customers);
        }

        // // GET : API/customer/:customerid
        /// <summary>
        /// Gets the customer by identifier asynchronous.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        [HttpGet("{customerId}")]
        [ProducesResponseType(typeof(CustomerReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCustomerByIdAsync(int customerId)
        {
            var customer = await _serviceCustomer.GetCustomerByIdAsync(customerId).ConfigureAwait(false);

            return Ok(customer);
        }

        /// <summary>
        /// Posts the customer asynchronous.
        /// </summary>
        /// <param name="newCustomer">The new customer.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(CustomerReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostCustomerAsync(CustomerAddDto newCustomer)
        {
            var customer = await _serviceCustomer.CreateCustomerAsync(newCustomer).ConfigureAwait(false);

            return Ok(customer);
        }

        /// <summary>
        /// Updates the customer by identifier asynchronous.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="updatedCustomer">The updated customer.</param>
        /// <returns></returns>
        [HttpPut("{customerId}")]
        [ProducesResponseType(typeof(CustomerReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateCustomerByIdAsync(int customerId, CustomerUpdateDto updatedCustomer)
        {
            var customer = await _serviceCustomer.UpdateCustomerByIdAsync(customerId, updatedCustomer).ConfigureAwait(false);

            return Ok(customer);
        }

        /// <summary>
        /// Deletes the customer by identifier asynchronous.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        [HttpDelete("{customerId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteCustomerByIdAsync(int customerId)
        {
            var customerIdDeleted = await _serviceCustomer.DeleteCustomerByIdAsync(customerId).ConfigureAwait(false);

            return Ok();
        }

    }
}
