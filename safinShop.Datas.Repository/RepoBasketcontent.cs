﻿using safinShop.Datas.Context.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository
{
    public class RepoBasketcontent : GenericRepo<Basketcontent>, IRepoBasketcontent
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RepoBasketcontent"/> class.
        /// </summary>
        /// <param name="safinDbContex"></param>
        public RepoBasketcontent(ISafinDbContext safinDbContext) : base(safinDbContext)
        {
        }
    }
}
