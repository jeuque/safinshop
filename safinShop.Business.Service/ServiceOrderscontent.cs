﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceOrderscontent : IServiceOrderscontent
    {
        private readonly IRepoOrderscontent _repoOrderscontent;
        private readonly IRepoProduct _repoProduct;
        private readonly IMapper _mapper;

        public ServiceOrderscontent(IRepoOrderscontent repoOrderscontent, IMapper mapper, IRepoProduct repoProduct)
        {
            _repoOrderscontent = repoOrderscontent;
            _mapper = mapper;
            _repoProduct = repoProduct;
        }

        public async Task<OrderscontentReadDto> CreateOrderscontentAsync(OrderscontentAddDto orderscontentToAdd)
        {
            var price = await _repoProduct.GetByKeyAsync(orderscontentToAdd.ProductId);
            var orderscontentAdd = _mapper.Map<Orderscontent>(orderscontentToAdd);
            orderscontentAdd.UnitePrice = price.ProductPrice;
            var orderscontent = await _repoOrderscontent.CreateElementAsync(orderscontentAdd).ConfigureAwait(false);

            return _mapper.Map<OrderscontentReadDto>(orderscontent);
        }

        public async Task<int> DeleteOrderscontentByIdAsync(int orderscontentId)
        {
            var orderscontentToDelete = await _repoOrderscontent.GetByKeyAsync(orderscontentId).ConfigureAwait(false);
            var orderscontent = await _repoOrderscontent.DeleteElementAsync(orderscontentToDelete).ConfigureAwait(false);

            return orderscontent.OrdersContentId;
        }

        public async Task<OrderscontentReadDto> GetOrderscontentByIdAsync(int orderscontentId)
        {
            var orderscontent = await _repoOrderscontent.GetByKeyAsyncDetails(orderscontentId).ConfigureAwait(false);
            return _mapper.Map<OrderscontentReadDto>(orderscontent);
        }

        public async Task<IEnumerable<OrderscontentReadDto>> GetListOrderscontentAsync()
        {
            var orderscontents = await _repoOrderscontent.GetAllAsyncDetails().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<OrderscontentReadDto>>(orderscontents);
        }

        public async Task<OrderscontentReadDto> UpdateOrderscontentByIdAsync(int orderscontentId, OrderscontentUpdateDto orderscontentToUpdate)
        {
            var orderscontentUpdate = _mapper.Map<Orderscontent>(orderscontentToUpdate);
            orderscontentUpdate.OrdersContentId = orderscontentId;
            var orderscontent = await _repoOrderscontent.UpdateElementAsync(orderscontentUpdate).ConfigureAwait(false);

            return _mapper.Map<OrderscontentReadDto>(orderscontent);
        }

        public async Task<IEnumerable<OrderscontentReadDto>> GetListOrderscontentByOrdersAsync(int ordersId)
        {
            var orderscontent = await _repoOrderscontent.GetByOrdersDetailsAsync(ordersId).ConfigureAwait(false);
            return _mapper.Map<IEnumerable<OrderscontentReadDto>>(orderscontent);
        }
    }
}
