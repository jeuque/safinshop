﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Customer
    {
        public Customer()
        {
            Basket = new HashSet<Basket>();
            Comment = new HashSet<Comment>();
            Linkcustadd = new HashSet<Linkcustadd>();
            Orders = new HashSet<Orders>();
        }

        public int CustomerId { get; set; }
        public string CustomerFisrtName { get; set; } = null!;
        public string CustomerLastName { get; set; } = null!;
        public string CustomerMail { get; set; } = null!;
        public string CustomerPassword { get; set; } = null!;

        public virtual ICollection<Basket> Basket { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<Linkcustadd> Linkcustadd { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
