﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceComment : IServiceComment
    {
        private readonly IRepoComment _repoComment;
        private readonly IMapper _mapper;

        public ServiceComment(IRepoComment repoComment, IMapper mapper)
        {
            _repoComment = repoComment;
            _mapper = mapper;
        }

        public async Task<CommentReadDto> CreateCommentAsync(CommentAddDto commentToAdd)
        {
            var commentAdd = _mapper.Map<Comment>(commentToAdd);
            var comment = await _repoComment.CreateElementAsync(commentAdd).ConfigureAwait(false);

            return _mapper.Map<CommentReadDto>(comment);
        }

        public async Task<int> DeleteCommentByIdAsync(int commentId)
        {
            var commentToDelete = await _repoComment.GetByKeyAsync(commentId).ConfigureAwait(false);
            var comment = await _repoComment.DeleteElementAsync(commentToDelete).ConfigureAwait(false);

            return comment.CommentId;
        }

        public async Task<CommentReadDto> GetCommentByIdAsync(int commentId)
        {
            var comment = await _repoComment.GetByKeyAsyncDetails(commentId).ConfigureAwait(false);
            return _mapper.Map<CommentReadDto>(comment);
        }

        public async Task<IEnumerable<CommentReadDto>> GetListCommentAsync()
        {
            var comments = await _repoComment.GetAllAsyncDetails().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<CommentReadDto>>(comments);
        }

        public async Task<CommentReadDto> UpdateCommentByIdAsync(int commentId, CommentUpdateDto commentToUpdate)
        {
            var commentUpdate = _mapper.Map<Comment>(commentToUpdate);
            commentUpdate.CommentId = commentId;
            var comment = await _repoComment.UpdateElementAsync(commentUpdate).ConfigureAwait(false);

            return _mapper.Map<CommentReadDto>(comment);
        }
    }
}
