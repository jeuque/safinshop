﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceLinkcustadd : IServiceLinkcustadd
    {
        private readonly IRepoLinkcustadd _repoLinkcustadd;
        private readonly IMapper _mapper;

        public ServiceLinkcustadd(IRepoLinkcustadd repoLinkcustadd, IMapper mapper)
        {
            _repoLinkcustadd = repoLinkcustadd;
            _mapper = mapper;
        }

        public async Task<LinkcustaddReadDto> CreateLinkcustaddAsync(LinkcustaddAddDto linkcustaddToAdd)
        {
            var linkcustaddAdd = _mapper.Map<Linkcustadd>(linkcustaddToAdd);
            var linkcustadd = await _repoLinkcustadd.CreateElementAsync(linkcustaddAdd).ConfigureAwait(false);

            return _mapper.Map<LinkcustaddReadDto>(linkcustadd);
        }

        public async Task<int> DeleteLinkcustaddByIdAsync(int linkcustaddId)
        {
            var linkcustaddToDelete = await _repoLinkcustadd.GetByKeyAsync(linkcustaddId).ConfigureAwait(false);
            var linkcustadd = await _repoLinkcustadd.DeleteElementAsync(linkcustaddToDelete).ConfigureAwait(false);

            return linkcustadd.LinkCustAddId;
        }

        public async Task<LinkcustaddReadDto> GetLinkcustaddByIdAsync(int linkcustaddId)
        {
            var linkcustadd = await _repoLinkcustadd.GetByKeyAsync(linkcustaddId).ConfigureAwait(false);
            return _mapper.Map<LinkcustaddReadDto>(linkcustadd);
        }

        public async Task<IEnumerable<LinkcustaddReadDto>> GetListLinkcustaddAsync()
        {
            var linkcustadds = await _repoLinkcustadd.GetAllAsync().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<LinkcustaddReadDto>>(linkcustadds);
        }

        public async Task<LinkcustaddReadDto> UpdateLinkcustaddByIdAsync(int linkcustaddId, LinkcustaddUpdateDto linkcustaddToUpdate)
        {
            var linkcustaddUpdate = _mapper.Map<Linkcustadd>(linkcustaddToUpdate);
            linkcustaddUpdate.LinkCustAddId = linkcustaddId;
            var linkcustadd = await _repoLinkcustadd.UpdateElementAsync(linkcustaddUpdate).ConfigureAwait(false);

            return _mapper.Map<LinkcustaddReadDto>(linkcustadd);
        }
    }
}
