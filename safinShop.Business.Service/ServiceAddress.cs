﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceAddress : IServiceAddress
    {
        private readonly IRepoAddress _repoAddress;
        private readonly IMapper _mapper;

        public ServiceAddress(IRepoAddress repoAddress, IMapper mapper)
        {
            _repoAddress = repoAddress;
            _mapper = mapper;
        }

        public async Task<AddressReadDto> CreateAddressAsync(AddressAddDto addressToAdd)
        {
            var addressAdd = _mapper.Map<Address>(addressToAdd);
            var address = await _repoAddress.CreateElementAsync(addressAdd).ConfigureAwait(false);

            return _mapper.Map<AddressReadDto>(address);
        }

        public async Task<int> DeleteAddressByIdAsync(int addressId)
        {
            var addressToDelete = await _repoAddress.GetByKeyAsync(addressId).ConfigureAwait(false);
            var address = await _repoAddress.DeleteElementAsync(addressToDelete).ConfigureAwait(false);

            return address.AddressId;
        }

        public async Task<AddressReadDto> GetAddressByIdAsync(int addressId)
        {
            var address = await _repoAddress.GetByKeyAsync(addressId).ConfigureAwait(false);
            return _mapper.Map<AddressReadDto>(address);
        }

        public async Task<IEnumerable<AddressReadDto>> GetListAddressAsync()
        {
            var addresses = await _repoAddress.GetAllAsync().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<AddressReadDto>>(addresses);
        }

        public async Task<AddressReadDto> UpdateAddressByIdAsync(int addressId, AddressUpdateDto addressToUpdate)
        {
            var addressUpdate = _mapper.Map<Address>(addressToUpdate);
            addressUpdate.AddressId = addressId;
            var address = await _repoAddress.UpdateElementAsync(addressUpdate).ConfigureAwait(false);

            return _mapper.Map<AddressReadDto>(address);
        }
    }
}
