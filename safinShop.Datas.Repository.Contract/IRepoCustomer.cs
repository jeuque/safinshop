﻿using safinShop.Datas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository.Contract
{
    public interface IRepoCustomer : IGenericRepo<Customer>
    {
    }
}
