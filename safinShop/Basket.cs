﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Basket
    {
        public Basket()
        {
            Basketcontent = new HashSet<Basketcontent>();
        }

        public int BasketId { get; set; }
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; } = null!;
        public virtual ICollection<Basketcontent> Basketcontent { get; set; }
    }
}
