﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderscontentController : ControllerBase
    {
        /// <summary>
        /// The service orderscontent
        /// </summary>
        private readonly IServiceOrderscontent _serviceOrderscontent;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderscontentController"/> class.
        /// </summary>
        /// <param name="serviceOrderscontent">The service orderscontent.</param>
        public OrderscontentController(IServiceOrderscontent serviceOrderscontent)
        {
            _serviceOrderscontent = serviceOrderscontent;
        }

        // GET : API/orderscontents
        /// <summary>
        /// Gets the orderscontents list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<OrderscontentReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetOrderscontentsAsync()
        {
            var orderscontents = await _serviceOrderscontent.GetListOrderscontentAsync().ConfigureAwait(false);

            return Ok(orderscontents);
        }

        // // GET : API/orderscontent/:orderscontentid
        /// <summary>
        /// Gets the orderscontent by identifier asynchronous.
        /// </summary>
        /// <param name="orderscontentId">The orderscontent identifier.</param>
        /// <returns></returns>
        [HttpGet("{orderscontentId}")]
        [ProducesResponseType(typeof(OrderscontentReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetOrderscontentByIdAsync(int orderscontentId)
        {
            var orderscontent = await _serviceOrderscontent.GetOrderscontentByIdAsync(orderscontentId).ConfigureAwait(false);

            return Ok(orderscontent);
        }

        /// <summary>
        /// Posts the orderscontent asynchronous.
        /// </summary>
        /// <param name="newOrderscontent">The new orderscontent.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(OrderscontentReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostOrderscontentAsync(OrderscontentAddDto newOrderscontent)
        {
            var orderscontent = await _serviceOrderscontent.CreateOrderscontentAsync(newOrderscontent).ConfigureAwait(false);

            return Ok(orderscontent);
        }

        /// <summary>
        /// Updates the orderscontent by identifier asynchronous.
        /// </summary>
        /// <param name="orderscontentId">The orderscontent identifier.</param>
        /// <param name="updatedOrderscontent">The updated orderscontent.</param>
        /// <returns></returns>
        [HttpPut("{orderscontentId}")]
        [ProducesResponseType(typeof(OrderscontentReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateOrderscontentByIdAsync(int orderscontentId, OrderscontentUpdateDto updatedOrderscontent)
        {
            var orderscontent = await _serviceOrderscontent.UpdateOrderscontentByIdAsync(orderscontentId, updatedOrderscontent).ConfigureAwait(false);

            return Ok(orderscontent);
        }

        /// <summary>
        /// Deletes the orderscontent by identifier asynchronous.
        /// </summary>
        /// <param name="orderscontentId">The orderscontent identifier.</param>
        /// <returns></returns>
        [HttpDelete("{orderscontentId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteOrderscontentByIdAsync(int orderscontentId)
        {
            var orderscontentIdDeleted = await _serviceOrderscontent.DeleteOrderscontentByIdAsync(orderscontentId).ConfigureAwait(false);

            return Ok();
        }

    }
}
