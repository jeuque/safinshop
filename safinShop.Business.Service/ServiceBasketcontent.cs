﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceBasketcontent : IServiceBasketcontent
    {
        private readonly IRepoBasketcontent _repoBasketcontent;
        private readonly IMapper _mapper;

        public ServiceBasketcontent(IRepoBasketcontent repoBasketcontent, IMapper mapper)
        {
            _repoBasketcontent = repoBasketcontent;
            _mapper = mapper;
        }

        public async Task<BasketcontentReadDto> CreateBasketcontentAsync(BasketcontentAddDto basketcontentToAdd)
        {
            var basketcontentAdd = _mapper.Map<Basketcontent>(basketcontentToAdd);
            var basketcontent = await _repoBasketcontent.CreateElementAsync(basketcontentAdd).ConfigureAwait(false);

            return _mapper.Map<BasketcontentReadDto>(basketcontent);
        }

        public async Task<int> DeleteBasketcontentByIdAsync(int basketcontentId)
        {
            var basketcontentToDelete = await _repoBasketcontent.GetByKeyAsync(basketcontentId).ConfigureAwait(false);
            var basketcontent = await _repoBasketcontent.DeleteElementAsync(basketcontentToDelete).ConfigureAwait(false);

            return basketcontent.BasketContentId;
        }

        public async Task<BasketcontentReadDto> GetBasketcontentByIdAsync(int basketcontentId)
        {
            var basketcontent = await _repoBasketcontent.GetByKeyAsync(basketcontentId).ConfigureAwait(false);
            return _mapper.Map<BasketcontentReadDto>(basketcontent);
        }

        public async Task<IEnumerable<BasketcontentReadDto>> GetListBasketcontentAsync()
        {
            var basketcontents = await _repoBasketcontent.GetAllAsync().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<BasketcontentReadDto>>(basketcontents);
        }

        public async Task<BasketcontentReadDto> UpdateBasketcontentByIdAsync(int basketcontentId, BasketcontentUpdateDto basketcontentToUpdate)
        {
            var basketcontentUpdate = _mapper.Map<Basketcontent>(basketcontentToUpdate);
            basketcontentUpdate.BasketContentId = basketcontentId;
            var basketcontent = await _repoBasketcontent.UpdateElementAsync(basketcontentUpdate).ConfigureAwait(false);

            return _mapper.Map<BasketcontentReadDto>(basketcontent);
        }
    }
}
