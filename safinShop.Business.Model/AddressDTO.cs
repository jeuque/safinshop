﻿namespace safinShop.Business.Model
{
    public class AddressBaseDTO
    {
        public string AddressName { get; set; }
        public string AddressZipCode { get; set; }
        public string AddressCity { get; set; }

    }

    public class AddressReadDto : AddressBaseDTO
    {
        public int AddressId { get; set; }
    }

    public class AddressAddDto : AddressBaseDTO
    {

    }
    public class AddressUpdateDto : AddressBaseDTO
    {
    }
}