﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceAddress
    {
        /// <summary>
        /// Creates the address.
        /// </summary>
        /// <param name="addressToAdd">The address to add.</param>
        /// <returns></returns>
        Task<AddressReadDto> CreateAddressAsync(AddressAddDto addressToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des addresses.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<AddressReadDto>> GetListAddressAsync();

        /// <summary>
        /// Gets the address by name asynchronous.
        /// </summary>
        /// <param name="addressName">Name of the address.</param>
        /// <returns></returns>
        Task<AddressReadDto> GetAddressByIdAsync(int addressId);

        /// <summary>
        /// Updates the address by identifier asynchronous.
        /// </summary>
        /// <param name="addressId">The address identifier.</param>
        /// <returns></returns>
        Task<AddressReadDto> UpdateAddressByIdAsync(int addressId, AddressUpdateDto addressToUpdate);

        /// <summary>
        /// Deletes the address by identifier asynchronous.
        /// </summary>
        /// <param name="addressId">The address identifier.</param>
        /// <returns></returns>
        Task<int> DeleteAddressByIdAsync(int addressId);
    }
}