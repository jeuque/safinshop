﻿using safinShop.Datas.Context.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository
{
    public class RepoCategory : GenericRepo<Category>, IRepoCategory
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RepoCategory"/> class.
        /// </summary>
        /// <param name="safinDbContex"></param>
        public RepoCategory(ISafinDbContext safinDbContext) : base(safinDbContext)
        {
        }
    }
}
