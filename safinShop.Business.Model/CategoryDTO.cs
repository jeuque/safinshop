﻿namespace safinShop.Business.Model
{
    public class CategoryBaseDTO
    {
        public string CategoryName { get; set; }

    }

    public class CategoryReadDto : CategoryBaseDTO
    {
        public int CategoryId { get; set; }
    }

    public class CategoryAddDto : CategoryBaseDTO
    {
    }
    public class CategoryUpdateDto : CategoryBaseDTO
    {
    }
}