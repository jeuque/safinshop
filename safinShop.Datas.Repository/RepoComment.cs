﻿using Microsoft.EntityFrameworkCore;
using safinShop.Datas.Context.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository
{
    public class RepoComment : GenericRepo<Comment>, IRepoComment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepoProduct"/> class.
        /// </summary>
        /// <param name="safinDbContex"></param>
        public RepoComment(ISafinDbContext safinDbContext) : base(safinDbContext)
        {
        }

        /// <summary>
        /// Gets all asynchronous details.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Comment>> GetAllAsyncDetails()
        {
            return await _table
                .Include(c => c.Customer)
                .Include(c => c.Product)
                    .ThenInclude(p => p.Category)
                .ToListAsync().ConfigureAwait(false);
        }

        public async Task<Comment?> GetByKeyAsyncDetails(int id)
        {
            return await _table
                .Include(c => c.Customer)
                .Include(c => c.Product)
                    .ThenInclude(p => p.Category)
                .FirstOrDefaultAsync(p => p.CommentId == id).ConfigureAwait(false);
        }
    }
}
