﻿using Microsoft.EntityFrameworkCore;
using safinShop.Datas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Context.Contract
{
    public interface ISafinDbContext : IDbContext
    {

        DbSet<Address> Address { get; set; }

        DbSet<Basket> Basket { get; set; }

        DbSet<Basketcontent> Basketcontent { get; set; }

        DbSet<Category> Category { get; set; }

        DbSet<Comment> Comment { get; set; }

        DbSet<Customer> Customer { get; set; }

        DbSet<Linkcustadd> Linkcustadd { get; set; }

        DbSet<Orders> Orders { get; set; }

        DbSet<Orderscontent> Orderscontent { get; set; }

        DbSet<Ordersstatus> Ordersstatus { get; set; }

        DbSet<Product> Product { get; set; }
    }
}
