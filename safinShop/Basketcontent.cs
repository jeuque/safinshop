﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Basketcontent
    {
        public int BasketContentId { get; set; }
        public int Quantity { get; set; }
        public int ProductId { get; set; }
        public int BasketId { get; set; }

        public virtual Basket Basket { get; set; } = null!;
        public virtual Product Product { get; set; } = null!;
    }
}
