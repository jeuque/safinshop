﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketController : ControllerBase
    {
        /// <summary>
        /// The service basket
        /// </summary>
        private readonly IServiceBasket _serviceBasket;

        /// <summary>
        /// Initializes a new instance of the <see cref="BasketController"/> class.
        /// </summary>
        /// <param name="serviceBasket">The service basket.</param>
        public BasketController(IServiceBasket serviceBasket)
        {
            _serviceBasket = serviceBasket;
        }

        // GET : API/baskets
        /// <summary>
        /// Gets the baskets list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<BasketReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetBasketsAsync()
        {
            var baskets = await _serviceBasket.GetListBasketAsync().ConfigureAwait(false);

            return Ok(baskets);
        }

        // // GET : API/basket/:basketid
        /// <summary>
        /// Gets the basket by identifier asynchronous.
        /// </summary>
        /// <param name="basketId">The basket identifier.</param>
        /// <returns></returns>
        [HttpGet("{basketId}")]
        [ProducesResponseType(typeof(BasketReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetBasketByIdAsync(int basketId)
        {
            var basket = await _serviceBasket.GetBasketByIdAsync(basketId).ConfigureAwait(false);

            return Ok(basket);
        }

        /// <summary>
        /// Posts the basket asynchronous.
        /// </summary>
        /// <param name="newBasket">The new basket.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(BasketReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostBasketAsync(BasketAddDto newBasket)
        {
            var basket = await _serviceBasket.CreateBasketAsync(newBasket).ConfigureAwait(false);

            return Ok(basket);
        }

        /// <summary>
        /// Updates the basket by identifier asynchronous.
        /// </summary>
        /// <param name="basketId">The basket identifier.</param>
        /// <param name="updatedBasket">The updated basket.</param>
        /// <returns></returns>
        [HttpPut("{basketId}")]
        [ProducesResponseType(typeof(BasketReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateBasketByIdAsync(int basketId, BasketUpdateDto updatedBasket)
        {
            var basket = await _serviceBasket.UpdateBasketByIdAsync(basketId, updatedBasket).ConfigureAwait(false);

            return Ok(basket);
        }

        /// <summary>
        /// Deletes the basket by identifier asynchronous.
        /// </summary>
        /// <param name="basketId">The basket identifier.</param>
        /// <returns></returns>
        [HttpDelete("{basketId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteBasketByIdAsync(int basketId)
        {
            var basketIdDeleted = await _serviceBasket.DeleteBasketByIdAsync(basketId).ConfigureAwait(false);

            return Ok();
        }

    }
}
