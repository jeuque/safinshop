﻿using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;

namespace safinShop.Business.Service
{
    public class ServiceCustomer : IServiceCustomer
    {

        private readonly IRepoCustomer _repoCustomer;
        private readonly IRepoLinkcustadd _repoLinkcustadd;
        private readonly IRepoAddress _repoAddress;


        public ServiceCustomer(IRepoCustomer repoCustomer, IRepoLinkcustadd repoLinkcustadd, IRepoAddress repoAddress)
        {
            _repoCustomer = repoCustomer;
            _repoLinkcustadd = repoLinkcustadd;
            _repoAddress = repoAddress;
        }

        /// <summary>
        /// Creates the customer asynchronous.
        /// </summary>
        /// <param name="customerToAdd">The customer to add.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<CustomerReadDto> CreateCustomerAsync(CustomerAddDto customerToAdd)
        {
            throw new NotImplementedException();

            // On doit gerer la creation d'addresse en parallele.

            Customer newCustomer = MapperCustomer.TransformCreateDtoToEntity(customerToAdd);

            var customer = await _repoCustomer.CreateElementAsync(newCustomer).ConfigureAwait(false);


            var link = await _repoLinkcustadd.GetByKeyAsync(customer.CustomerId).ConfigureAwait(false);
            var address = await _repoAddress.GetByKeyAsync(link.AddressId).ConfigureAwait(false);
            return MapperCustomer.TransformCustomerToReadDto(customer, address);

            //TODO reussir a retrourner l'id du dernier element créé et non pas 0.
        }

        /// <summary>
        /// Gets the customer by identifier.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        public async Task<CustomerReadDto> GetCustomerByIdAsync(int customerId)
        {
            var customer = await _repoCustomer.GetByKeyAsync(customerId).ConfigureAwait(false);
            var link = await _repoLinkcustadd.GetByKeyAsync(customer.CustomerId).ConfigureAwait(false);
            var address = await _repoAddress.GetByKeyAsync(link.AddressId).ConfigureAwait(false);
            return MapperCustomer.TransformCustomerToReadDto(customer, address);
        }

        /// <summary>
        /// Gets a list of customers.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<CustomerReadDto>> GetListCustomerAsync()
        {
            var customers = await _repoCustomer.GetAllAsync().ConfigureAwait(false);

            List<CustomerReadDto> customerDtos = new();

            foreach (var customer in customers)
            {
                var link = await _repoLinkcustadd.GetByKeyAsync(customer.CustomerId).ConfigureAwait(false);
                var address = await _repoAddress.GetByKeyAsync(link.AddressId).ConfigureAwait(false);
                customerDtos.Add(MapperCustomer.TransformCustomerToReadDto(customer, address));
            }

            return customerDtos;
        }

        /// <summary>
        /// Updates the customer by identifier.
        /// </summary>
        /// <param name="customerToUpdate"></param>
        /// <returns></returns>
        public async Task<CustomerReadDto> UpdateCustomerByIdAsync(int customerId, CustomerUpdateDto customerToUpdate)
        {
            Customer newCustomer = MapperCustomer.TransformUpdateDtoToEntity(customerToUpdate, customerId);

            var customer = await _repoCustomer.UpdateElementAsync(newCustomer).ConfigureAwait(false);
            var link = await _repoLinkcustadd.GetByKeyAsync(customer.CustomerId).ConfigureAwait(false);
            var address = await _repoAddress.GetByKeyAsync(link.AddressId).ConfigureAwait(false);

            return MapperCustomer.TransformCustomerToReadDto(customer, address);
        }

        /// <summary>
        /// Deletes the customer by identifier.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
       public async Task<int> DeleteCustomerByIdAsync(int customerId)
        {
            var customerToDelete = await _repoCustomer.GetByKeyAsync(customerId).ConfigureAwait(false);
            var customer = await _repoCustomer.DeleteElementAsync(customerToDelete).ConfigureAwait(false);

            return customer.CustomerId;
        }
    }
}