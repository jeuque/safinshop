﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Datas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            // Address Mapper
            CreateMap<Address, AddressReadDto>();
            CreateMap<AddressAddDto, Address>();
            CreateMap<AddressUpdateDto, Address>();

            // Customer Mapper
            CreateMap<Customer, CustomerReadDto>();
            CreateMap<Customer, CustomerReadCompDto>();
            CreateMap<CustomerAddDto, Customer>();
            CreateMap<CustomerUpdateDto, Customer>();

            // Category Mapper
            CreateMap<Category, CategoryReadDto>();
            CreateMap<CategoryAddDto, Category>();
            CreateMap<CategoryUpdateDto, Category>();

            // Product Mapper
            CreateMap<Product, ProductReadDto>();
            CreateMap<ProductAddDto, Product>();
            CreateMap<ProductUpdateDto, Product>();

            // Product Mapper
            CreateMap<Linkcustadd, LinkcustaddReadDto>();
            CreateMap<LinkcustaddAddDto, Linkcustadd>();
            CreateMap<LinkcustaddUpdateDto, Linkcustadd>();

            // Comment Mapper
            CreateMap<Comment, CommentReadDto>();
            CreateMap<CommentAddDto, Comment>();
            CreateMap<CommentUpdateDto, Comment>();

            // BasketContent Mapper
            CreateMap<Basketcontent, BasketcontentReadDto>();
            CreateMap<BasketcontentAddDto, Basketcontent>();
            CreateMap<BasketcontentUpdateDto, Basketcontent>();

            // Basket Mapper
            CreateMap<Basket, BasketReadDto>();
            CreateMap<BasketAddDto, Basket>();
            CreateMap<BasketUpdateDto, Basket>();

            // Orders Mapper
            CreateMap<Orders, OrdersReadDto>();
            CreateMap<Orders, OrdersReadDetailsDto>();
            CreateMap<OrdersAddDto, Orders>();
            CreateMap<OrdersUpdateDto, Orders>();

            // Ordersstatus Mapper
            CreateMap<Ordersstatus, OrdersstatusReadDto>();
            CreateMap<OrdersstatusAddDto, Ordersstatus>();
            CreateMap<OrdersstatusUpdateDto, Ordersstatus>();

            // Orderscontent Mapper
            CreateMap<Orderscontent, OrderscontentReadDto>();
            CreateMap<Orderscontent, OrderscontentReadDetailsDto>();
            CreateMap<OrderscontentAddDto, Orderscontent>();
            CreateMap<OrderscontentUpdateDto, Orderscontent>();

        }
    }
}
