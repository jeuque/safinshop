﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        /// <summary>
        /// The service category
        /// </summary>
        private readonly IServiceCategory _serviceCategory;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryController"/> class.
        /// </summary>
        /// <param name="serviceCategory">The service category.</param>
        public CategoryController(IServiceCategory serviceCategory)
        {
            _serviceCategory = serviceCategory;
        }

        // GET : API/categories
        /// <summary>
        /// Gets the categories list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<CategoryReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCategoriesAsync()
        {
            var categories = await _serviceCategory.GetListCategoryAsync().ConfigureAwait(false);

            return Ok(categories);
        }

        // GET : API/category/:categoryid
        /// <summary>
        /// Gets the category by identifier asynchronous.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        [HttpGet("{categoryId}")]
        [ProducesResponseType(typeof(CategoryReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCategoryByIdAsync(int categoryId)
        {
            var category = await _serviceCategory.GetCategoryByIdAsync(categoryId).ConfigureAwait(false);

            return Ok(category);
        }

        /// <summary>
        /// Posts the category asynchronous.
        /// </summary>
        /// <param name="newCategory">The new category.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(CategoryReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostCategoryAsync(CategoryAddDto newCategory)
        {
            var category = await _serviceCategory.CreateCategoryAsync(newCategory).ConfigureAwait(false);

            return Ok(category);
        }

        /// <summary>
        /// Updates the category by identifier asynchronous.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="updatedCategory">The updated category.</param>
        /// <returns></returns>
        [HttpPut("{categoryId}")]
        [ProducesResponseType(typeof(CategoryReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateCategoryByIdAsync(int categoryId, CategoryUpdateDto updatedCategory)
        {
            var category = await _serviceCategory.UpdateCategoryByIdAsync(categoryId, updatedCategory).ConfigureAwait(false);

            return Ok(category);
        }

        /// <summary>
        /// Deletes the category by identifier asynchronous.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        [HttpDelete("{categoryId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteCategoryByIdAsync(int categoryId)
        {
            var categoryIdDeleted = await _serviceCategory.DeleteCategoryByIdAsync(categoryId).ConfigureAwait(false);

            return Ok();
        }

    }
}
