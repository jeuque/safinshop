﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        /// <summary>
        /// The service address
        /// </summary>
        private readonly IServiceAddress _serviceAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressController"/> class.
        /// </summary>
        /// <param name="serviceAddress">The service address.</param>
        public AddressController(IServiceAddress serviceAddress)
        {
            _serviceAddress = serviceAddress;
        }

        // GET : API/addresses
        /// <summary>
        /// Gets the addresses list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<AddressReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetAddressesAsync()
        {
            var addresses = await _serviceAddress.GetListAddressAsync().ConfigureAwait(false);

            return Ok(addresses);
        }

        // // GET : API/address/:addressid
        /// <summary>
        /// Gets the address by identifier asynchronous.
        /// </summary>
        /// <param name="addressId">The address identifier.</param>
        /// <returns></returns>
        [HttpGet("{addressId}")]
        [ProducesResponseType(typeof(AddressReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetAddressByIdAsync(int addressId)
        {
            var address = await _serviceAddress.GetAddressByIdAsync(addressId).ConfigureAwait(false);

            return Ok(address);
        }

        /// <summary>
        /// Posts the address asynchronous.
        /// </summary>
        /// <param name="newAddress">The new address.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(AddressReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostAddressAsync(AddressAddDto newAddress)
        {
            var address = await _serviceAddress.CreateAddressAsync(newAddress).ConfigureAwait(false);

            return Ok(address);
        }

        /// <summary>
        /// Updates the address by identifier asynchronous.
        /// </summary>
        /// <param name="addressId">The address identifier.</param>
        /// <param name="updatedAddress">The updated address.</param>
        /// <returns></returns>
        [HttpPut("{addressId}")]
        [ProducesResponseType(typeof(AddressReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateAddressByIdAsync(int addressId, AddressUpdateDto updatedAddress)
        {
            var address = await _serviceAddress.UpdateAddressByIdAsync(addressId, updatedAddress).ConfigureAwait(false);

            return Ok(address);
        }

        /// <summary>
        /// Deletes the address by identifier asynchronous.
        /// </summary>
        /// <param name="addressId">The address identifier.</param>
        /// <returns></returns>
        [HttpDelete("{addressId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteAddressByIdAsync(int addressId)
        {
            var addressIdDeleted = await _serviceAddress.DeleteAddressByIdAsync(addressId).ConfigureAwait(false);

            return Ok();
        }

    }
}
