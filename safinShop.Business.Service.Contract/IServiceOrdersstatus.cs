﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceOrdersstatus
    {
        /// <summary>
        /// Creates the ordersstatus.
        /// </summary>
        /// <param name="orderstatusToAdd">The ordersstatus to add.</param>
        /// <returns></returns>
        Task<OrdersstatusReadDto> CreateOrdersstatusAsync(OrdersstatusAddDto ordersstatusToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des Ordersstatus.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<OrdersstatusReadDto>> GetListOrdersstatusAsync();

        /// <summary>
        /// Gets the orderstatus by name asynchronous.
        /// </summary>
        /// <param name="orderstatusName">Name of the ordersstatus.</param>
        /// <returns></returns>
        Task<OrdersstatusReadDto> GetOrdersstatusByIdAsync(int ordersstatusId);

        /// <summary>
        /// Updates the ordersstatus by identifier asynchronous.
        /// </summary>
        /// <param name="ordersstatusId">The ordersstatus identifier.</param>
        /// <returns></returns>
        Task<OrdersstatusReadDto> UpdateOrdersstatusByIdAsync(int ordersstatusId, OrdersstatusUpdateDto ordersstatusToUpdate);

        /// <summary>
        /// Deletes the ordersstatus by identifier asynchronous.
        /// </summary>
        /// <param name="ordersstatusId">The ordersstatus identifier.</param>
        /// <returns></returns>
        Task<int> DeleteOrdersstatusByIdAsync(int ordersstatusId);
    }
}