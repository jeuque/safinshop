﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Product
    {
        public Product()
        {
            Basketcontent = new HashSet<Basketcontent>();
            Comment = new HashSet<Comment>();
            Orderscontent = new HashSet<Orderscontent>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; } = null!;
        public double ProductPrice { get; set; }
        public int ProductStock { get; set; }
        public string ProductDescription { get; set; } = null!;
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; } = null!;
        public virtual ICollection<Basketcontent> Basketcontent { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<Orderscontent> Orderscontent { get; set; }
    }
}
