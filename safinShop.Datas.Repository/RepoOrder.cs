﻿using Microsoft.EntityFrameworkCore;
using safinShop.Datas.Context.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository
{
    public class RepoOrder : GenericRepo<Orders>, IRepoOrder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RepoOrder"/> class.
        /// </summary>
        /// <param name="safinDbContex"></param>
        public RepoOrder(ISafinDbContext safinDbContext) : base(safinDbContext)
        {
        }
        /// <summary>
        /// Gets all asynchronous details.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Orders>> GetAllAsyncDetails()
        {
            return await _table
                .Include(o => o.Customer)
                .Include(o => o.OrdersStatus)
                .Include(o => o.Orderscontent)
                .ToListAsync().ConfigureAwait(false);
        }

        public async Task<Orders?> GetByKeyAsyncDetails(int id)
        {
            return await _table
                .Include(o => o.Customer)
                .Include(o => o.OrdersStatus)
                .Include(o => o.Orderscontent)
                .FirstOrDefaultAsync(o => o.OrdersId == id).ConfigureAwait(false);
        }
    }
}
