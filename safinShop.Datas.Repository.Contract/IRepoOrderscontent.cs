﻿using safinShop.Datas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Datas.Repository.Contract
{
    public interface IRepoOrderscontent : IGenericRepo<Orderscontent>
    {
        Task<IEnumerable<Orderscontent>> GetByOrdersDetailsAsync(int ordersId);
        Task<IEnumerable<Orderscontent>> GetAllAsyncDetails();
        Task<Orderscontent?> GetByKeyAsyncDetails(int id);
    }
}
