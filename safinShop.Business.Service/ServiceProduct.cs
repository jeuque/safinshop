﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceProduct : IServiceProduct
    {
        private readonly IRepoProduct _repoProduct;
        private readonly IMapper _mapper;

        public ServiceProduct(IRepoProduct repoProduct, IMapper mapper)
        {
            _repoProduct = repoProduct;
            _mapper = mapper;
        }

        public async Task<ProductReadDto> CreateProductAsync(ProductAddDto productToAdd)
        {
            var productAdd = _mapper.Map<Product>(productToAdd);
            var product = await _repoProduct.CreateElementAsync(productAdd).ConfigureAwait(false);

            return _mapper.Map<ProductReadDto>(product);
        }

        public async Task<int> DeleteProductByIdAsync(int productId)
        {
            var productToDelete = await _repoProduct.GetByKeyAsync(productId).ConfigureAwait(false);
            var product = await _repoProduct.DeleteElementAsync(productToDelete).ConfigureAwait(false);

            return product.ProductId;
        }

        public async Task<ProductReadDto> GetProductByIdAsync(int productId)
        {
            var product = await _repoProduct.GetByKeyAsyncDetails(productId).ConfigureAwait(false);
            return _mapper.Map<ProductReadDto>(product);
        }

        public async Task<IEnumerable<ProductReadDto>> GetListProductAsync()
        {
            var products = await _repoProduct.GetAllAsyncDetails().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<ProductReadDto>>(products);
        }

        public async Task<ProductReadDto> UpdateProductByIdAsync(int productId, ProductUpdateDto productToUpdate)
        {
            var productUpdate = _mapper.Map<Product>(productToUpdate);
            productUpdate.ProductId = productId;
            var product = await _repoProduct.UpdateElementAsync(productUpdate).ConfigureAwait(false);

            return _mapper.Map<ProductReadDto>(product);
        }
    }
}
