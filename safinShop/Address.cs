﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Address
    {
        public Address()
        {
            Linkcustadd = new HashSet<Linkcustadd>();
        }

        public int AddressId { get; set; }
        public string AddressName { get; set; } = null!;
        public string AddressZipCode { get; set; } = null!;
        public string AddressCity { get; set; } = null!;

        public virtual ICollection<Linkcustadd> Linkcustadd { get; set; }
    }
}
