﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketcontentController : ControllerBase
    {
        /// <summary>
        /// The service basketcontent
        /// </summary>
        private readonly IServiceBasketcontent _serviceBasketcontent;

        /// <summary>
        /// Initializes a new instance of the <see cref="BasketcontentController"/> class.
        /// </summary>
        /// <param name="serviceBasketcontent">The service basketcontent.</param>
        public BasketcontentController(IServiceBasketcontent serviceBasketcontent)
        {
            _serviceBasketcontent = serviceBasketcontent;
        }

        // GET : API/basketcontents
        /// <summary>
        /// Gets the basketcontents list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<BasketcontentReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetBasketcontentsAsync()
        {
            var basketcontents = await _serviceBasketcontent.GetListBasketcontentAsync().ConfigureAwait(false);

            return Ok(basketcontents);
        }

        // // GET : API/basketcontent/:basketcontentid
        /// <summary>
        /// Gets the basketcontent by identifier asynchronous.
        /// </summary>
        /// <param name="basketcontentId">The basketcontent identifier.</param>
        /// <returns></returns>
        [HttpGet("{basketcontentId}")]
        [ProducesResponseType(typeof(BasketcontentReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetBasketcontentByIdAsync(int basketcontentId)
        {
            var basketcontent = await _serviceBasketcontent.GetBasketcontentByIdAsync(basketcontentId).ConfigureAwait(false);

            return Ok(basketcontent);
        }

        /// <summary>
        /// Posts the basketcontent asynchronous.
        /// </summary>
        /// <param name="newBasketcontent">The new basketcontent.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(BasketcontentReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostBasketcontentAsync(BasketcontentAddDto newBasketcontent)
        {
            var basketcontent = await _serviceBasketcontent.CreateBasketcontentAsync(newBasketcontent).ConfigureAwait(false);

            return Ok(basketcontent);
        }

        /// <summary>
        /// Updates the basketcontent by identifier asynchronous.
        /// </summary>
        /// <param name="basketcontentId">The basketcontent identifier.</param>
        /// <param name="updatedBasketcontent">The updated basketcontent.</param>
        /// <returns></returns>
        [HttpPut("{basketcontentId}")]
        [ProducesResponseType(typeof(BasketcontentReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateBasketcontentByIdAsync(int basketcontentId, BasketcontentUpdateDto updatedBasketcontent)
        {
            var basketcontent = await _serviceBasketcontent.UpdateBasketcontentByIdAsync(basketcontentId, updatedBasketcontent).ConfigureAwait(false);

            return Ok(basketcontent);
        }

        /// <summary>
        /// Deletes the basketcontent by identifier asynchronous.
        /// </summary>
        /// <param name="basketcontentId">The basketcontent identifier.</param>
        /// <returns></returns>
        [HttpDelete("{basketcontentId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteBasketcontentByIdAsync(int basketcontentId)
        {
            var basketcontentIdDeleted = await _serviceBasketcontent.DeleteBasketcontentByIdAsync(basketcontentId).ConfigureAwait(false);

            return Ok();
        }

    }
}
