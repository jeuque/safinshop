﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceCustomer
    {
        /// <summary>
        /// Creates the customers.
        /// </summary>
        /// <param name="customersToAdd">The customers to add.</param>
        /// <returns></returns>
        Task<CustomerReadDto> CreateCustomerAsync(CustomerAddDto customerToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des customers.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<CustomerReadDto>> GetListCustomerAsync();

        /// <summary>
        /// Gets the customers by name asynchronous.
        /// </summary>
        /// <param name="customersName">Name of the customers.</param>
        /// <returns></returns>
        Task<CustomerReadDto> GetCustomerByIdAsync(int customerId);

        /// <summary>
        /// Updates the customer by identifier asynchronous.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        Task<CustomerReadDto> UpdateCustomerByIdAsync(int customerId, CustomerUpdateDto customerToUpdate);

        /// <summary>
        /// Deletes the customer by identifier asynchronous.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        Task<int> DeleteCustomerByIdAsync(int customerId);
    }
}