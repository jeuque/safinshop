﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        /// <summary>
        /// The service comment
        /// </summary>
        private readonly IServiceComment _serviceComment;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentController"/> class.
        /// </summary>
        /// <param name="serviceComment">The service comment.</param>
        public CommentController(IServiceComment serviceComment)
        {
            _serviceComment = serviceComment;
        }

        // GET : API/comments
        /// <summary>
        /// Gets the comments list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<CommentReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCommentsAsync()
        {
            var comments = await _serviceComment.GetListCommentAsync().ConfigureAwait(false);

            return Ok(comments);
        }

        // // GET : API/comment/:commentid
        /// <summary>
        /// Gets the comment by identifier asynchronous.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <returns></returns>
        [HttpGet("{commentId}")]
        [ProducesResponseType(typeof(CommentReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetCommentByIdAsync(int commentId)
        {
            var comment = await _serviceComment.GetCommentByIdAsync(commentId).ConfigureAwait(false);

            return Ok(comment);
        }

        /// <summary>
        /// Posts the comment asynchronous.
        /// </summary>
        /// <param name="newComment">The new comment.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(CommentReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostCommentAsync(CommentAddDto newComment)
        {
            var comment = await _serviceComment.CreateCommentAsync(newComment).ConfigureAwait(false);

            return Ok(comment);
        }

        /// <summary>
        /// Updates the comment by identifier asynchronous.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <param name="updatedComment">The updated comment.</param>
        /// <returns></returns>
        [HttpPut("{commentId}")]
        [ProducesResponseType(typeof(CommentReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateCommentByIdAsync(int commentId, CommentUpdateDto updatedComment)
        {
            var comment = await _serviceComment.UpdateCommentByIdAsync(commentId, updatedComment).ConfigureAwait(false);

            return Ok(comment);
        }

        /// <summary>
        /// Deletes the comment by identifier asynchronous.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <returns></returns>
        [HttpDelete("{commentId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteCommentByIdAsync(int commentId)
        {
            var commentIdDeleted = await _serviceComment.DeleteCommentByIdAsync(commentId).ConfigureAwait(false);

            return Ok();
        }

    }
}
