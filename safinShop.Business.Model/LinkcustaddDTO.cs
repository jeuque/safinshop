﻿namespace safinShop.Business.Model
{
    public class LinkcustaddBaseDTO
    {
        public int CustomerId { get; set; }
        public int AddressId { get; set; }

    }

    public class LinkcustaddReadDto : LinkcustaddBaseDTO
    {
        public int LinkcustaddId { get; set; }
    }

    public class LinkcustaddAddDto : LinkcustaddBaseDTO
    {
    }
    public class LinkcustaddUpdateDto : LinkcustaddBaseDTO
    {
    }
}