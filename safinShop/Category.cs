﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Category
    {
        public Category()
        {
            Product = new HashSet<Product>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; } = null!;

        public virtual ICollection<Product> Product { get; set; }
    }
}
