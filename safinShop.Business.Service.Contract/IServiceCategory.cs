﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceCategory
    {
        /// <summary>
        /// Creates the category.
        /// </summary>
        /// <param name="categoryToAdd">The category to add.</param>
        /// <returns></returns>
        Task<CategoryReadDto> CreateCategoryAsync(CategoryAddDto categoryToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des categories.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<CategoryReadDto>> GetListCategoryAsync();

        /// <summary>
        /// Gets the category by name asynchronous.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <returns></returns>
        Task<CategoryReadDto> GetCategoryByIdAsync(int categoryId);

        /// <summary>
        /// Updates the category by identifier asynchronous.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        Task<CategoryReadDto> UpdateCategoryByIdAsync(int categoryId, CategoryUpdateDto categoryToUpdate);

        /// <summary>
        /// Deletes the category by identifier asynchronous.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        Task<int> DeleteCategoryByIdAsync(int categoryId);
    }
}