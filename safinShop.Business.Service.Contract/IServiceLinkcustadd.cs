﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceLinkcustadd
    {
        /// <summary>
        /// Creates the linkcustadd.
        /// </summary>
        /// <param name="linkcustaddToAdd">The linkcustadd to add.</param>
        /// <returns></returns>
        Task<LinkcustaddReadDto> CreateLinkcustaddAsync(LinkcustaddAddDto linkcustaddToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des linkcustadds.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<LinkcustaddReadDto>> GetListLinkcustaddAsync();

        /// <summary>
        /// Gets the linkcustadd by name asynchronous.
        /// </summary>
        /// <param name="linkcustaddName">Name of the linkcustadd.</param>
        /// <returns></returns>
        Task<LinkcustaddReadDto> GetLinkcustaddByIdAsync(int linkcustaddId);

        /// <summary>
        /// Updates the linkcustadd by identifier asynchronous.
        /// </summary>
        /// <param name="linkcustaddId">The customer identifier.</param>
        /// <returns></returns>
        Task<LinkcustaddReadDto> UpdateLinkcustaddByIdAsync(int customerId, LinkcustaddUpdateDto linkcustaddToUpdate);

        /// <summary>
        /// Deletes the linkcustadd by identifier asynchronous.
        /// </summary>
        /// <param name="linkcustaddId">The linkcustadd identifier.</param>
        /// <returns></returns>
        Task<int> DeleteLinkcustaddByIdAsync(int linkcustaddId);
    }
}