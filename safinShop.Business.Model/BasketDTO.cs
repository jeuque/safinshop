﻿namespace safinShop.Business.Model
{
    public class BasketBaseDTO
    {
        public int CustomerId { get; set; }

    }

    public class BasketReadDto : BasketBaseDTO
    {
        public int BasketId { get; set; }
    }

    public class BasketAddDto : BasketBaseDTO
    {
    }
    public class BasketUpdateDto : BasketBaseDTO
    {
    }
}