﻿namespace safinShop.Business.Model
{
    public class BasketcontentBaseDTO
    {
        public int Quantity { get; set; }
        public int ProductId { get; set; }
        public int BasketId { get; set; }
    }

    public class BasketcontentReadDto : BasketcontentBaseDTO
    {
        public int BasketcontentId { get; set; }
    }

    public class BasketcontentAddDto : BasketcontentBaseDTO
    {
    }
    public class BasketcontentUpdateDto : BasketcontentBaseDTO
    {
    }
}