﻿using Microsoft.AspNetCore.Mvc;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;

namespace safinShop.Applications.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersstatusController : ControllerBase
    {
        /// <summary>
        /// The service ordersstatus
        /// </summary>
        private readonly IServiceOrdersstatus _serviceOrdersstatus;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersstatusController"/> class.
        /// </summary>
        /// <param name="serviceOrdersstatus">The service ordersstatus.</param>
        public OrdersstatusController(IServiceOrdersstatus serviceOrdersstatus)
        {
            _serviceOrdersstatus = serviceOrdersstatus;
        }

        // GET : API/ordersstatuss
        /// <summary>
        /// Gets the ordersstatus list asynchronous.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<OrdersstatusReadDto>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetOrdersstatussAsync()
        {
            var ordersstatus = await _serviceOrdersstatus.GetListOrdersstatusAsync().ConfigureAwait(false);

            return Ok(ordersstatus);
        }

        // // GET : API/ordersstatus/:ordersstatusid
        /// <summary>
        /// Gets the ordersstatus by identifier asynchronous.
        /// </summary>
        /// <param name="ordersstatusId">The ordersstatus identifier.</param>
        /// <returns></returns>
        [HttpGet("{ordersstatusId}")]
        [ProducesResponseType(typeof(OrdersstatusReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> GetOrdersstatusByIdAsync(int ordersstatusId)
        {
            var ordersstatus = await _serviceOrdersstatus.GetOrdersstatusByIdAsync(ordersstatusId).ConfigureAwait(false);

            return Ok(ordersstatus);
        }

        /// <summary>
        /// Posts the ordersstatus asynchronous.
        /// </summary>
        /// <param name="newOrdersstatus">The new ordersstatus.</param>
        /// <returns></returns>
        [HttpPost()]
        [ProducesResponseType(typeof(OrdersstatusReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> PostOrdersstatusAsync(OrdersstatusAddDto newOrdersstatus)
        {
            var ordersstatus = await _serviceOrdersstatus.CreateOrdersstatusAsync(newOrdersstatus).ConfigureAwait(false);

            return Ok(ordersstatus);
        }

        /// <summary>
        /// Updates the ordersstatus by identifier asynchronous.
        /// </summary>
        /// <param name="ordersstatusId">The ordersstatus identifier.</param>
        /// <param name="updatedOrdersstatus">The updated ordersstatus.</param>
        /// <returns></returns>
        [HttpPut("{ordersstatusId}")]
        [ProducesResponseType(typeof(OrdersstatusReadDto), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> UpdateOrdersstatusByIdAsync(int ordersstatusId, OrdersstatusUpdateDto updatedOrdersstatus)
        {
            var ordersstatus = await _serviceOrdersstatus.UpdateOrdersstatusByIdAsync(ordersstatusId, updatedOrdersstatus).ConfigureAwait(false);

            return Ok(ordersstatus);
        }

        /// <summary>
        /// Deletes the ordersstatus by identifier asynchronous.
        /// </summary>
        /// <param name="ordersstatusId">The ordersstatus identifier.</param>
        /// <returns></returns>
        [HttpDelete("{ordersstatusId}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> DeleteOrdersstatusByIdAsync(int ordersstatusId)
        {
            var ordersstatusIdDeleted = await _serviceOrdersstatus.DeleteOrdersstatusByIdAsync(ordersstatusId).ConfigureAwait(false);

            return Ok();
        }

    }
}
