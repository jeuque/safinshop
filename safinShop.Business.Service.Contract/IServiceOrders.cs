﻿using safinShop.Business.Model;

namespace safinShop.Business.Service.Contract
{
    public interface IServiceOrders
    {
        /// <summary>
        /// Creates the orders.
        /// </summary>
        /// <param name="ordersToAdd">The orders to add.</param>
        /// <returns></returns>
        Task<OrdersReadDto> CreateOrdersAsync(OrdersAddDto ordersToAdd);

        /// <summary>
        /// Cette méthode permet de récupérer la liste des orders.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<OrdersReadDto>> GetListOrdersAsync();

        /// <summary>
        /// Gets the orders by name asynchronous.
        /// </summary>
        /// <param name="ordersName">Name of the orders.</param>
        /// <returns></returns>
        Task<OrdersReadDetailsDto> GetOrdersByIdAsync(int ordersId);

        /// <summary>
        /// Updates the orders by identifier asynchronous.
        /// </summary>
        /// <param name="ordersId">The orders identifier.</param>
        /// <returns></returns>
        Task<OrdersReadDto> UpdateOrdersByIdAsync(int ordersId, OrdersUpdateDto ordersToUpdate);

        /// <summary>
        /// Deletes the orders by identifier asynchronous.
        /// </summary>
        /// <param name="ordersId">The orders identifier.</param>
        /// <returns></returns>
        Task<int> DeleteOrdersByIdAsync(int ordersId);
    }
}