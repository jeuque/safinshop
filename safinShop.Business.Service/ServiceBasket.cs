﻿using AutoMapper;
using safinShop.Business.Model;
using safinShop.Business.Service.Contract;
using safinShop.Datas.Entities;
using safinShop.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace safinShop.Business.Service
{
    public class ServiceBasket : IServiceBasket
    {
        private readonly IRepoBasket _repoBasket;
        private readonly IMapper _mapper;

        public ServiceBasket(IRepoBasket repoBasket, IMapper mapper)
        {
            _repoBasket = repoBasket;
            _mapper = mapper;
        }

        public async Task<BasketReadDto> CreateBasketAsync(BasketAddDto basketToAdd)
        {
            var basketAdd = _mapper.Map<Basket>(basketToAdd);
            var basket = await _repoBasket.CreateElementAsync(basketAdd).ConfigureAwait(false);

            return _mapper.Map<BasketReadDto>(basket);
        }

        public async Task<int> DeleteBasketByIdAsync(int basketId)
        {
            var basketToDelete = await _repoBasket.GetByKeyAsync(basketId).ConfigureAwait(false);
            var basket = await _repoBasket.DeleteElementAsync(basketToDelete).ConfigureAwait(false);

            return basket.BasketId;
        }

        public async Task<BasketReadDto> GetBasketByIdAsync(int basketId)
        {
            var basket = await _repoBasket.GetByKeyAsync(basketId).ConfigureAwait(false);
            return _mapper.Map<BasketReadDto>(basket);
        }

        public async Task<IEnumerable<BasketReadDto>> GetListBasketAsync()
        {
            var baskets = await _repoBasket.GetAllAsync().ConfigureAwait(false);
            return _mapper.Map<IEnumerable<BasketReadDto>>(baskets);
        }

        public async Task<BasketReadDto> UpdateBasketByIdAsync(int basketId, BasketUpdateDto basketToUpdate)
        {
            var basketUpdate = _mapper.Map<Basket>(basketToUpdate);
            basketUpdate.BasketId = basketId;
            var basket = await _repoBasket.UpdateElementAsync(basketUpdate).ConfigureAwait(false);

            return _mapper.Map<BasketReadDto>(basket);
        }
    }
}
