﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using safinShop.Datas.Context.Contract;

namespace safinShop.Datas.Entities.safinShop.Datas.Context
{
    public partial class SafinDBContext : DbContext, ISafinDbContext
    {
        public SafinDBContext()
        {
        }

        public SafinDBContext(DbContextOptions<SafinDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Address { get; set; } = null!;
        public virtual DbSet<Basket> Basket { get; set; } = null!;
        public virtual DbSet<Basketcontent> Basketcontent { get; set; } = null!;
        public virtual DbSet<Category> Category { get; set; } = null!;
        public virtual DbSet<Comment> Comment { get; set; } = null!;
        public virtual DbSet<Customer> Customer { get; set; } = null!;
        public virtual DbSet<Linkcustadd> Linkcustadd { get; set; } = null!;
        public virtual DbSet<Orders> Orders { get; set; } = null!;
        public virtual DbSet<Orderscontent> Orderscontent { get; set; } = null!;
        public virtual DbSet<Ordersstatus> Ordersstatus { get; set; } = null!;
        public virtual DbSet<Product> Product { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=localhost;database=safindb;port=3306;user=root", Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.7.36-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8_general_ci")
                .HasCharSet("utf8");

            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("address");

                entity.Property(e => e.AddressId)
                    .HasColumnType("int(11)")
                    .HasColumnName("addressId");

                entity.Property(e => e.AddressCity)
                    .HasMaxLength(255)
                    .HasColumnName("addressCity");

                entity.Property(e => e.AddressName)
                    .HasMaxLength(255)
                    .HasColumnName("addressName");

                entity.Property(e => e.AddressZipCode)
                    .HasMaxLength(255)
                    .HasColumnName("addressZipCode");
            });

            modelBuilder.Entity<Basket>(entity =>
            {
                entity.ToTable("basket");

                entity.HasIndex(e => e.CustomerId, "FKCustomerIdBasket");

                entity.Property(e => e.BasketId)
                    .HasColumnType("int(11)")
                    .HasColumnName("basketId");

                entity.Property(e => e.CustomerId)
                    .HasColumnType("int(11)")
                    .HasColumnName("customerId");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Basket)
                    .HasForeignKey(d => d.CustomerId);
            });

            modelBuilder.Entity<Basketcontent>(entity =>
            {
                entity.ToTable("basketcontent");

                entity.HasIndex(e => e.BasketId, "FKBasketIdContent");

                entity.HasIndex(e => e.ProductId, "FKProductIdContentBasket");

                entity.Property(e => e.BasketContentId)
                    .HasColumnType("int(11)")
                    .HasColumnName("basketContentId");

                entity.Property(e => e.BasketId)
                    .HasColumnType("int(11)")
                    .HasColumnName("basketId");

                entity.Property(e => e.ProductId)
                    .HasColumnType("int(11)")
                    .HasColumnName("productId");

                entity.Property(e => e.Quantity)
                    .HasColumnType("int(11)")
                    .HasColumnName("quantity");

                entity.HasOne(d => d.Basket)
                    .WithMany(p => p.Basketcontent)
                    .HasForeignKey(d => d.BasketId);
                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Basketcontent)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("category");

                entity.Property(e => e.CategoryId)
                    .HasColumnType("int(11)")
                    .HasColumnName("categoryId");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(255)
                    .HasColumnName("categoryName");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.ToTable("comment");

                entity.HasIndex(e => e.CustomerId, "FKCustomerIdComment");

                entity.HasIndex(e => e.ProductId, "FKProductIdComment");

                entity.Property(e => e.CommentId)
                    .HasColumnType("int(11)")
                    .HasColumnName("commentId");

                entity.Property(e => e.CommentContent)
                    .HasMaxLength(255)
                    .HasColumnName("commentContent");

                entity.Property(e => e.CommentMark)
                    .HasColumnType("int(1)")
                    .HasColumnName("commentMark");

                entity.Property(e => e.CommentTittle)
                    .HasMaxLength(255)
                    .HasColumnName("commentTittle");

                entity.Property(e => e.CustomerId)
                    .HasColumnType("int(11)")
                    .HasColumnName("customerId");

                entity.Property(e => e.ProductId)
                    .HasColumnType("int(11)")
                    .HasColumnName("productId");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Comment)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Comment)
                    .HasForeignKey(d => d.ProductId);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("customer");

                entity.Property(e => e.CustomerId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever()
                    .HasColumnName("customerId");

                entity.Property(e => e.CustomerFisrtName)
                    .HasMaxLength(255)
                    .HasColumnName("customerFisrtName");

                entity.Property(e => e.CustomerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("customerLastName");

                entity.Property(e => e.CustomerMail)
                    .HasMaxLength(255)
                    .HasColumnName("customerMail");

                entity.Property(e => e.CustomerPassword)
                    .HasMaxLength(255)
                    .HasColumnName("customerPassword");
            });

            modelBuilder.Entity<Linkcustadd>(entity =>
            {
                entity.ToTable("linkcustadd");

                entity.HasIndex(e => e.AddressId, "FKAddressId");

                entity.HasIndex(e => e.CustomerId, "FKCustomerIdAddress");

                entity.Property(e => e.LinkCustAddId)
                    .HasColumnType("int(11)")
                    .HasColumnName("linkCustAddId");

                entity.Property(e => e.AddressId)
                    .HasColumnType("int(11)")
                    .HasColumnName("addressId");

                entity.Property(e => e.CustomerId)
                    .HasColumnType("int(11)")
                    .HasColumnName("customerId");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Linkcustadd)
                    .HasForeignKey(d => d.AddressId);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Linkcustadd)
                    .HasForeignKey(d => d.CustomerId);
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.OrdersId)
                    .HasName("PRIMARY");

                entity.ToTable("orders");

                entity.HasIndex(e => e.CustomerId, "FKCustomerId");

                entity.HasIndex(e => e.OrdersStatusId, "FKOrdersStatus");

                entity.Property(e => e.OrdersId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ordersId");

                entity.Property(e => e.CustomerId)
                    .HasColumnType("int(11)")
                    .HasColumnName("customerId");

                entity.Property(e => e.OrdersDate)
                    .HasColumnType("datetime")
                    .HasColumnName("ordersDate");

                entity.Property(e => e.OrdersStatusId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ordersStatusId");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CustomerId);
                entity.HasOne(d => d.OrdersStatus)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.OrdersStatusId);
            });

            modelBuilder.Entity<Orderscontent>(entity =>
            {
                entity.ToTable("orderscontent");

                entity.HasIndex(e => e.OrdersId, "FKOrdersIdContent");

                entity.HasIndex(e => e.ProductId, "FKProductIdContent");

                entity.Property(e => e.OrdersContentId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ordersContentId");

                entity.Property(e => e.OrdersId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ordersId");

                entity.Property(e => e.ProductId)
                    .HasColumnType("int(11)")
                    .HasColumnName("productId");

                entity.Property(e => e.Quantity)
                    .HasColumnType("int(11)")
                    .HasColumnName("quantity");

                entity.Property(e => e.UnitePrice).HasColumnName("unitePrice");

                entity.HasOne(d => d.Orders)
                    .WithMany(p => p.Orderscontent)
                    .HasForeignKey(d => d.OrdersId);
                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Orderscontent)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Ordersstatus>(entity =>
            {
                entity.HasKey(e => e.OrdersStatusId)
                    .HasName("PRIMARY");

                entity.ToTable("ordersstatus");

                entity.Property(e => e.OrdersStatusId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ordersStatusId");

                entity.Property(e => e.OrdersStatusName)
                    .HasMaxLength(255)
                    .HasColumnName("ordersStatusName");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("product");

                entity.HasIndex(e => e.CategoryId, "categoryId");

                entity.Property(e => e.ProductId)
                    .HasColumnType("int(11)")
                    .HasColumnName("productId");

                entity.Property(e => e.CategoryId)
                    .HasColumnType("int(11)")
                    .HasColumnName("categoryId");

                entity.Property(e => e.ProductDescription)
                    .HasColumnType("text")
                    .HasColumnName("productDescription");

                entity.Property(e => e.ProductName)
                    .HasMaxLength(255)
                    .HasColumnName("productName");

                entity.Property(e => e.ProductPrice).HasColumnName("productPrice");

                entity.Property(e => e.ProductStock).HasColumnType("int(11)");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
