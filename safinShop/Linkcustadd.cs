﻿using System;
using System.Collections.Generic;

namespace safinShop.Datas.Entities
{
    public partial class Linkcustadd
    {
        public int LinkCustAddId { get; set; }
        public int CustomerId { get; set; }
        public int AddressId { get; set; }

        public virtual Address Address { get; set; } = null!;
        public virtual Customer Customer { get; set; } = null!;
    }
}
